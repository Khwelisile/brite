<?php

session_start();

include 'dbh.php';
 
$first = mysqli_real_escape_string($conn, $_POST['first']);
$last = mysqli_real_escape_string($conn, $_POST['last']);
$email = mysqli_real_escape_string($conn, $_POST['email']);
$uid = mysqli_real_escape_string($conn, $_POST['uid']);
$pwd = mysqli_real_escape_string($conn, $_POST['pwd']);

if (empty($first) || empty($last) || empty($uid) || empty($pwd) || empty($email)) {
	header("Location: ../index.php?index=empty");
	exit();
}
else{
	//check if inputs are valid
	/*if (!preg_match("/^[a-zA-Z]*$/", $first)) {
		header("Location: ../index.php?index=email");
		exit();
	}*/
	//else{
		//check if email valid
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
		{

			header("Location: ../index.php?index=email");
			exit();
		}
		else{
			$sql = "SELECT * FROM users WHERE uid = '$uid'";
			$result = mysqli_query($conn, $sql);
			$resultCheck = mysqli_num_rows($result);

			if ($resultCheck > 0) 
			{
				echo	"<script>
						alert(\"Account already exists.\");
						window.location=\"../index.php\";
						</script>";

				//header("Location: ../index.php?index=usertaken");
				exit();
			}
			else{
				//hashing the password
				$hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

				$sql = "INSERT INTO users (first, last, email, uid, pwd) VALUES ('$first', '$last', '$email', '$uid', '$hashedPwd');";
				mysqli_query($conn, $sql);

				echo	"<script>
						alert(\"Account successfully created.\");
						window.location=\"../loginPage.php\";
						</script>";

				//header("Location: ../index.php?index.php=success");
				exit();
			}
		}
	//}
}


/*$sql = "INSERT INTO users (first, last, email, uid, pwd) 
VALUES ('$first', '$last', '$email', '$uid', '$pwd')";

$result = mysqli_query($conn, $sql);

header("Location: ../index.php");*/