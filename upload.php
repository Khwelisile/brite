<?php

require_once('php-scripts/dbh.php');

$location = "uploads/";

if (!isset($_POST['upload'])) 
{
    echo "Error.";
} 
else 
{
    $file = $_FILES['file'];
    
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileType = $_FILES['file']['type'];

    
    //date("F d Y H:i:s", getlastmod())
    
    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('docx', 'jpg', 'jpeg', 'png', 'pdf');

    if (!in_array($fileActualExt, $allowed)) 
    {
        header("Location: file.php?upload=type");
    } 
    else 
    {
        if ($fileError === 0) 
        {
            if ($fileSize > 500000000) 
            {
                    header("Location: file.php?upload=large");
            } 
            else
             {
                $lastMod = filemtime($fileName);
                $fileNameNew = uniqid('', TRUE).".".$fileActualExt;
                $fileDestination = "uploads/".$fileName;
                move_uploaded_file($fileTmpName, $fileDestination);
                $sql = "INSERT INTO `upload` (`name`, `size`, `type`, `location`) VALUES ('$fileName', '$fileSize', '$fileType', '$location$fileName')";
                $res = mysqli_query($conn, $sql);
                header("Location: file.php?upload=success");
            }
        } 
        else 
        {
            header("Location: file.php?upload=error");
        }
    }
}