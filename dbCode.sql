CREATE TABLE users (
  id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  first varchar(255) NOT NULL,
  last varchar(255) NOT NULL,
  uid varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  pwd varchar(255) NOT NULL
)


CREATE TABLE `upload` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL DEFAULT 'File Transfer',
  `disposition` varchar(20) NOT NULL DEFAULT 'attachment',
  `expires` varchar(10) NOT NULL DEFAULT '0',
  `cache` varchar(20) NOT NULL DEFAULT 'must-revalidate',
  `pragma` varchar(10) NOT NULL DEFAULT 'public',
  `location` varchar(255) NOT NULL
);


CREATE TABLE profileimg(
 id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 userid int(11) NOT NULL,
 status int(11) NOT NULL
);